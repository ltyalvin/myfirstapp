package com.example.myfirstapp.adaptors;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myfirstapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO
 * fixme asdasd
 */
public class TotoRvAdapter extends RecyclerView.Adapter<TotoRvAdapter.VH> {

    private final List<String> totoNumbers = new ArrayList<>();

    public TotoRvAdapter(Deletecallback deletecallback) {
        this.deletecallback = deletecallback;
    }

    private final Deletecallback deletecallback;

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item, parent, false);
        return new VH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        String tototNumber = totoNumbers.get(position);
        holder.updateView(tototNumber);
    }

    @Override
    public int getItemCount() {
        return totoNumbers.size();
    }

    private void deleteBtn(int adapterPosition) {
        totoNumbers.remove(adapterPosition);
        notifyDataSetChanged();
        deletecallback.onDelete(totoNumbers);
    }

    public void addItem(String newNumber) {
        totoNumbers.add(newNumber);
        notifyItemInserted(totoNumbers.size() - 1);
    }

    public void addItemList(List<String> newNumber) {
        totoNumbers.addAll(newNumber);
        notifyDataSetChanged();
    }

    public void clear() {
        totoNumbers.clear();
        notifyDataSetChanged();
    }

    public String getLastEntry() {

        return totoNumbers.isEmpty()?"Click Button":totoNumbers.get(totoNumbers.size()-1);
    }


    public class VH extends RecyclerView.ViewHolder {

        final TextView tvTotoNumber;
        final ImageView deleteBtn;

        VH(@NonNull View itemView) {
            super(itemView);

            tvTotoNumber = itemView.findViewById(R.id.cv_toto_number);
            deleteBtn = itemView.findViewById(R.id.delete_toto);

            deleteBtn.setOnClickListener(v -> deleteBtn(getAdapterPosition()));
        }

        void updateView(String totoNumber) {
            tvTotoNumber.setText(totoNumber);
        }
    }

    public interface Deletecallback{
        void onDelete(List<String> list);
    }
}
