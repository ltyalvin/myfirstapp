package com.example.myfirstapp.logic;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class creates numbers.
 */
public class NumGenerator {
    private final Random random = new Random();

    /**
     * generate random numbbers from 1-49 with size - arg
     * @param sizes
     * @return
     */
    private Set<Integer> getNumbers(int size){
        Set<Integer> result = new TreeSet<>();
        while(result.size() < size)
        {
            int number = random.nextInt(49) + 1;
            result.add(number);
        }
        return result;
    }

    public String getNumbersStrring(int size)
    {
        Set<Integer> result = getNumbers(size);
        StringBuilder number = new StringBuilder();
        for (int num : result) {
            number.append(num).append(" ");
        }
        return number.toString();
    }
}
