package com.example.myfirstapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myfirstapp.logic.NumGenerator;

/**
 * A placeholder fragment containing a simple view.
 */
class TotoActivityFragment_beforeViewManagers extends Fragment {

    private static final String TAG = "Alvin_TotoF";

    private RecyclerView rv;
    private TextView tvResult;
    private Spinner spinner;
    private Button btn;
    private final NumGenerator numgen = new NumGenerator();
    private final int totosize = 6;
    private String lastgeneratedNumber = "Click Button";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        setRetainInstance(true);
        //return inflater.inflate(R.layout.content_main2, container, false);

        View view = inflater.inflate(R.layout.content_main2, container, false);

        tvResult = view.findViewById(R.id.tb_result);
        btn = view.findViewById(R.id.button);
        spinner = view.findViewById(R.id.spinner);
        rv = view.findViewById(R.id.recyclerview);

        tvResult.setText(lastgeneratedNumber);
        btn.setOnClickListener(v -> handlebuttonclick());

        return view;
    }

    private void handlebuttonclick() {
        lastgeneratedNumber = numgen.getNumbersStrring(totosize);
        tvResult.setText(lastgeneratedNumber);
    }

    public TotoActivityFragment_beforeViewManagers() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
    }

    
}
