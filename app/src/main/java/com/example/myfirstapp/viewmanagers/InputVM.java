package com.example.myfirstapp.viewmanagers;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.myfirstapp.R;

public class InputVM {

    private Spinner spinner;
    private Button btn;
    private final BtnClickedCallBack btnClickedCallBack;
    private SpinnerCallBack spinnerCallBack = null;

    private final String[] tototype = {"ODINARY","SYS 7","SYS 8","SYS 9","SYS 10","SYS 11"};

    public InputVM(BtnClickedCallBack btnClickedCallBack) {
        this.btnClickedCallBack = btnClickedCallBack;
    }

    public void setupview(View view, SpinnerCallBack spinnerCallBack) {

        this.spinnerCallBack= spinnerCallBack;

        btn = view.findViewById(R.id.button);
        spinner = view.findViewById(R.id.spinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(view.getContext(),
                android.R.layout.simple_spinner_dropdown_item,tototype);
        spinner.setAdapter(adapter);

        btn.setOnClickListener(v -> btnClickedCallBack.onButtonClicked());

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int totoSize = position + 6;
                spinnerCallBack.onspinnerSelected(totoSize);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public interface BtnClickedCallBack {
        void onButtonClicked();
    }

    public interface SpinnerCallBack {
        void onspinnerSelected(int size);
    }

}
