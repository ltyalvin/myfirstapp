package com.example.myfirstapp.viewmanagers;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.myfirstapp.R;
import com.example.myfirstapp.adaptors.TotoRvAdapter;

import java.util.List;

public class OutputVM {
    private RecyclerView rv;
    private TextView tvResult;

    private final TotoRvAdapter adapter;

    public OutputVM(List<String> oldresult, TotoRvAdapter.Deletecallback deletecallback) {
        adapter = new TotoRvAdapter(deletecallback);
        adapter.addItemList(oldresult);
    }


    public void setupview(View view) {
        rv = view.findViewById(R.id.recyclerview);
        tvResult = view.findViewById(R.id.tb_result);

        rv.setAdapter(adapter);

        tvResult.setText(adapter.getLastEntry());
    }

    public void setText(String lastgeneratedNumber) {
        tvResult.setText(lastgeneratedNumber);

        adapter.addItem(lastgeneratedNumber);
    }

    public void clear() {
        adapter.clear();
        tvResult.setText("Click Number");
    }
}
