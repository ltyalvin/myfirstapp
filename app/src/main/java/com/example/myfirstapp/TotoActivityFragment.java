package com.example.myfirstapp;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myfirstapp.logic.NumGenerator;
import com.example.myfirstapp.viewmanagers.InputVM;
import com.example.myfirstapp.viewmanagers.OutputVM;

import java.util.ArrayList;
import java.util.List;

import logic.FileHandler;

/**
 * A placeholder fragment containing a simple view.
 */
class TotoActivityFragment extends Fragment {

    private static final String TAG = "Alvin_TotoF";



    private final NumGenerator numgen = new NumGenerator();
    private int totosize = 6;
    private String lastgeneratedNumber = "Click Button";

    private InputVM inputVM;
    private OutputVM outputVM;

    private final FileHandler fh = new FileHandler();
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        setRetainInstance(true);
        //return inflater.inflate(R.layout.content_main2, container, false);

        View view = inflater.inflate(R.layout.content_main2, container, false);


        inputVM.setupview(view, this::totosizechanged);
        outputVM.setupview(view);

        //outputVM.setText(lastgeneratedNumber);


        return view;
    }

    private void totosizechanged(int newsize) {
        totosize = newsize;

    }

    private void handlebuttonclick() {
        lastgeneratedNumber = numgen.getNumbersStrring(totosize);
        outputVM.setText(lastgeneratedNumber);

        fh.writefile(getContext(),lastgeneratedNumber);
    }

    public TotoActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");

        List<String> oldresult = fh.readfile(getContext());

        inputVM = new InputVM(()->handlebuttonclick());
        outputVM = new OutputVM(oldresult,this::handleupdatefile);
    }

    private void handleupdatefile(List<String> list) {
        fh.writefile(getContext(),list);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
    }


    public void clear() {
        outputVM.clear();
        fh.writefile(getContext(),new ArrayList<>());
    }
}
