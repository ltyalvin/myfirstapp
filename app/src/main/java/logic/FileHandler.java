package logic;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class FileHandler {
    private final String FILE_NAME = "totohistory.txt";
    private final String TAG = "Alvin_File_Handler";

    public void writefile(Context context, List<String> List) {
        File file = new File(context.getFilesDir(), FILE_NAME);

        try (
                FileWriter fw = new FileWriter(file);
                BufferedWriter bw = new BufferedWriter(fw,4000);
        ){
            for (String s : List) {
                bw.write(s);
                bw.write("\n");

            }
        } catch (Exception e) {
            Log.d(TAG, "readfile: NO GOOD cannot open: " + e);
        }
    }

    public void writefile(Context context, String s) {
        File file = new File(context.getFilesDir(), FILE_NAME);

        try (
                FileWriter fw = new FileWriter(file,true);
                BufferedWriter bw = new BufferedWriter(fw,4000);
        ){
            bw.write(s);
            bw.write("\n");
        } catch (Exception e) {
            Log.d(TAG, "readfile: NO GOOD cannot open: " + e);
        }
    }

    public List<String> readfile(Context context) {
        File file = new File(context.getFilesDir(), FILE_NAME);
        List<String> result = new ArrayList<>();

        try(
                FileReader fr = new FileReader(file);
                BufferedReader br = new BufferedReader(fr,4000);
                ){
            while (br.ready()){
                String line = br.readLine();
                result.add(line);
            }
        }catch(Exception e)
        {
            Log.d(TAG, "readfile: NO GOOD cannot read: "+ e);
        }
        return result;
    }
}
